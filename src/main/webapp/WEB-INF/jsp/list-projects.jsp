<%@ include file="common/header.jspf"%>
<%@ include file="common/navigation.jspf"%>

<div class="container">
<form method="GET">

		<table class="table table-striped">
			<caption>Your Projects are</caption>
			<thead>
				<tr>
					<th>Name</th>
					<th>ID</th>
					<!-- <th>Branchs</th> -->
				</tr>
			</thead>
			<tbody>

				<c:forEach items="${projects}" var="project">
					<tr>
						<td><input type="checkbox" name="selected"
							value="${project.id}"> ${project.name}<BR></td>
						<td>${project.id}</td>

						<%-- <td><select name="projects">
							<c:forEach items="${todo.branchs}" var="branch">
								<option value="${todo.id}">${branch.name}</option>
							</c:forEach>
					</select></td> --%>

					</tr>
				</c:forEach>
			</tbody>
		</table>
		<input class="btn btn-success" type="submit" value="Create"
			onclick="form.action='/create-branch';">
	    <input class="btn btn-warning" type="submit" value="Delete"
			onclick="form.action='/delete-branch';">

	</form>
</div>

<%@ include file="common/footer.jspf"%>
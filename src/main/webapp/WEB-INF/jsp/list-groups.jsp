<%@ include file="common/header.jspf"%>
<%@ include file="common/navigation.jspf"%>

<div class="container">
	<form method="GET">

		<table class="table table-striped">
			<caption>Your Groups are</caption>
			<thead>
				<tr>
					<th></th>
					<th>Group</th>
					<th>ID</th>
				</tr>
			</thead>
			<tbody>

				<c:forEach items="${groups}" var="group">
					<tr>
						<td><input type="checkbox" name="selectedgroup"
							value="${group.id}"></td>
						<td><a class="button" href="/list-projects?id=${group.id}">${group.name}</a></td>
						<td>${group.id}</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<input class="btn btn-success" type="submit" value="Create"
			onclick="form.action='/create-branch';"> 
			<input class="btn btn-warning" type="submit" value="Delete"
			onclick="form.action='/delete-branch';">

	</form>

<!-- 	<div>
		<a class="button" href="/add-project">Add Projects</a>
	</div> -->
</div>

<%@ include file="common/footer.jspf"%>
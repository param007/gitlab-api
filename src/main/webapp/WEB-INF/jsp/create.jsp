
<%@ include file="common/header.jspf"%>
<%@ include file="common/navigation.jspf"%>
<div class="container">
	<form:form method="post" commandName="create">
		<form:hidden path="id" />
		<fieldset class="form-group">
			<form:label path="name">BranchName</form:label>
			<form:input path="name" type="text" class="form-control"
				required="required" />
			<form:errors path="name" cssClass="text-warning" />
		</fieldset>

		<fieldset class="form-group">
			<form:label path="ref">RefBranch</form:label>
			<form:input path="ref" type="text" class="form-control"
				required="required" />
			<form:errors path="ref" cssClass="text-warning" />
		</fieldset>

		<button type="submit" class="btn btn-success">Create</button>
	</form:form>
</div>
<%@ include file="common/footer.jspf"%>

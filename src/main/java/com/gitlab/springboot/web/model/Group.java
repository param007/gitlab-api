package com.gitlab.springboot.web.model;

import java.util.ArrayList;
import java.util.List;

public class Group {
	private int id;
	private String name;
	private List<Projct> projects = new ArrayList<>();

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Projct> getProjects() {
		return projects;
	}

	public void setProjects(List<Projct> projects) {
		this.projects = projects;
	}

	public Group() {
		super();
	}

	public Group(int id, String name, List<Projct> projects) {
		super();
		this.id = id;
		this.name = name;
		this.projects = projects;
	}

	@Override
	public String toString() {
		return String.format("Group [id=%s, name=%s, projects=%s]", id, name, projects);
	}

}
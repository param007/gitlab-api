package com.gitlab.springboot.web.model;

public class Branch {
	private int id;

	// @Size(min=10, message="Enter at least 10 Characters...")
	private String name;
	private String ref;

	public String getref() {
		return ref;
	}

	public void setref(String ref) {
		this.ref = ref;
	}

	public Branch() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Branch(int id, String name, String ref) {
		super();
		this.id = id;
		this.name = name;
		this.ref = ref;

	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Branch other = (Branch) obj;
		if (id != other.id) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return String.format("Branch [id=%s, name=%s, ref=%s]", id, name, ref);
	}

}
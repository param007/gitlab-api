package com.gitlab.springboot.web.model;

import java.util.ArrayList;
import java.util.List;

public class Projct {
	private int id;
	private String name;
	List<Branch> branchs = new ArrayList<Branch>();

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Branch> getBranchs() {
		return branchs;
	}

	public void setBranchs(List<Branch> branchs) {
		this.branchs = branchs;
	}

	public Projct() {
		super();
	}

	public Projct(int id, String name, List<Branch> branchs) {
		super();
		this.id = id;
		this.name = name;
		this.branchs = branchs;
	}

	@Override
	public String toString() {
		return String.format("Project [id=%s, name=%s, branchs=%s]", id, name, branchs);
	}

}
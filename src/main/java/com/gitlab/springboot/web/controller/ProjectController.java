package com.gitlab.springboot.web.controller;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.gitlab4j.api.GitLabApiException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import com.gitlab.springboot.web.model.Branch;
import com.gitlab.springboot.web.model.Projct;
import com.gitlab.springboot.web.service.ProjectService;

@Controller
public class ProjectController {

	@Autowired
	ProjectService service;

	String user;

	@RequestMapping(value = "/list-groups", method = RequestMethod.GET)
	public String showGroups(ModelMap model) throws GitLabApiException {
		service.addProjects();
		model.put("groups", service.retrieveGroups());
		System.out.println("groups-------->" + model);
		return "list-groups";
	}

/*	@RequestMapping(value = "/add-project", method = RequestMethod.GET)
	public String showAddTodoPage(ModelMap model) throws GitLabApiException {
		model.addAttribute("todo", new Projct(0, "name", null));
		service.addProjects();
		return "redirect:/list-groups";
	}*/

	@RequestMapping(value = "/list-projects", method = RequestMethod.GET)
	public String showProjects(@RequestParam int id, ModelMap model) throws GitLabApiException {
		model.put("projects", service.listProjects(id));
		System.out.println("projects-------->" + model);
		return "list-projects";

	}

	@RequestMapping(value = "/create-branch", method = RequestMethod.GET)
	public String createPage(ModelMap model) {
		model.addAttribute("create", new Branch(0, "Name", "Ref"));
		return "create";
	}

	@RequestMapping(value = "/create-branch", method = RequestMethod.POST)
	public String create(ModelMap model, HttpServletRequest request, Branch create) throws GitLabApiException {

		String select[] = request.getParameterValues("selected");
		String selectgroup[] = request.getParameterValues("selectedgroup");

		if (select != null) {
			int[] value = new int[select.length];
			for (int i = 0; i < select.length; i++) {
				value[i] = Integer.parseInt(select[i]);
			}

			for (int i = 0; i < value.length; i++) {
				service.createBranch(value[i], create.getName(), create.getref());
			}
		}

		if (selectgroup != null) {
			int[] groupsid = new int[selectgroup.length];
			for (int i = 0; i < selectgroup.length; i++) {
				groupsid[i] = Integer.parseInt(selectgroup[i]);
			}

			for (int i = 0; i < groupsid.length; i++) {
				List<Projct> projects = service.listProjects(groupsid[i]);
				for (Projct projct : projects) {
					service.createBranch(projct.getId(), create.getName(), create.getref());
				}
			}
		}
		return "redirect:/list-groups";
	}

	@RequestMapping(value = "/delete-branch", method = RequestMethod.GET)
	public String deletePage(ModelMap model) {
		model.addAttribute("delete", new Branch(0, "Name", "Ref"));
		return "delete";
	}

	@RequestMapping(value = "/delete-branch", method = RequestMethod.POST)
	public String delete(HttpServletRequest request, ModelMap model, Branch delete) throws GitLabApiException {

		String select[] = request.getParameterValues("selected");
		String selectgroup[] = request.getParameterValues("selectedgroup");

		if (select != null) {
			int[] value = new int[select.length];
			for (int i = 0; i < select.length; i++) {
				value[i] = Integer.parseInt(select[i]);
			}

			for (int i = 0; i < value.length; i++) {
				service.deleteBranch(value[i], delete.getName());
			}
		}

		if (selectgroup != null) {
			int[] groupsid = new int[selectgroup.length];
			for (int i = 0; i < selectgroup.length; i++) {
				groupsid[i] = Integer.parseInt(selectgroup[i]);
			}

			for (int i = 0; i < groupsid.length; i++) {
				List<Projct> projects = service.listProjects(groupsid[i]);
				for (Projct projct : projects) {
					service.deleteBranch(projct.getId(), delete.getName());
				}
			}
		}

		return "redirect:/list-groups";
	}

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String loginpage(ModelMap model) {
		model.addAttribute("login", "login");
		return "login";
	}

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String login(HttpServletRequest request) {
		user = request.getParameter("username");
		String pass = request.getParameter("password");

		service.login(user, pass);

		System.out.println(user + " " + pass);

		return "redirect:/list-groups";
	}
}

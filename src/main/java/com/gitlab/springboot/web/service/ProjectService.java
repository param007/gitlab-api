package com.gitlab.springboot.web.service;

import java.util.ArrayList;
import java.util.List;
import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.Pager;
import org.springframework.stereotype.Service;
import com.gitlab.springboot.web.model.Branch;
import com.gitlab.springboot.web.model.Group;
import com.gitlab.springboot.web.model.Projct;

@Service
public class ProjectService {

	private static List<Group> groups = new ArrayList<>();

	GitLabApi gitLabApi;
	String Username;
	String Password;
	String Url = "https://gitlab.verizon.com";
	//String Url = "https://gitlab.com";


	public void login(String user, String pass) {

		Username = user;
		Password = pass;
		try {
			gitLabApi = GitLabApi.login(Url, Username, Password);
		} catch (GitLabApiException e) {

			String err = e.getMessage();
			System.out.println(err);
		}
	}

	public List<Group> retrieveGroups() {

		return groups;
	}

	public void addProjects() throws GitLabApiException {

		groups.removeAll(groups);
		Pager<org.gitlab4j.api.models.Group> groupPager = gitLabApi.getGroupApi().getGroups(200);
		while (groupPager.hasNext()) {
			for (org.gitlab4j.api.models.Group group : groupPager.next()) {
				groups.add(new Group(group.getId(), group.getName(), null));

			}
		}
	}

	public List<Branch> listBranch(int id) throws GitLabApiException {
		List<Branch> branch1 = new ArrayList<>();

		Pager<org.gitlab4j.api.models.Branch> projectPager = gitLabApi.getRepositoryApi().getBranches(id, 200);
		while (projectPager.hasNext()) {
			for (org.gitlab4j.api.models.Branch branch : projectPager.next()) {
				branch1.add(new Branch(id, branch.getName(), "default"));
			}
		}

		return branch1;
	}

	public List<Projct> listProjects(int id) throws GitLabApiException {
		List<Projct> project1 = new ArrayList<>();

		Pager<org.gitlab4j.api.models.Project> projectPager = gitLabApi.getGroupApi().getProjects(id, 200);
		while (projectPager.hasNext()) {
			for (org.gitlab4j.api.models.Project project : projectPager.next()) {
				project1.add(new Projct(project.getId(), project.getName(), null));
			}
		}

		return project1;
	}

	public void deleteBranch(int id, String name) throws GitLabApiException {

		gitLabApi.getRepositoryApi().deleteBranch(id, name);
	}

	public void createBranch(int id, String name, String ref) throws GitLabApiException {

		gitLabApi.getRepositoryApi().createBranch(id, name, ref);

	}

}
